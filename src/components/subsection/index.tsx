import * as React from "react";
import classNames from "classnames";
import "./style.scss"

export interface SubsectionProps {
    title?: string;
    children: React.ReactNode;
    smallTopMargin?: boolean;
}

export function Subsection(props: SubsectionProps) {
    const subsectionClass = classNames(
        "subsection",
        {
            "small-top-margin": props.smallTopMargin
        }
    )

    return (
        <div className={subsectionClass}>
            {
                props.title ?
                    <h3 className={"title"}>{props.title}</h3>
                    : undefined
            }
            {props.children}
        </div>
    );
}