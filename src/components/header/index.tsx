import * as React from "react";
import { Link } from "react-router-dom";
import Logo from "../../images/illustrations/logo.svg";
import "./style.scss";

export function Header() {
    return (
        <header>
            <Link
                to={"/"}
            >
                <img
                    src={Logo}
                    alt={"Logo with Kylon's name set in children's letter blocks"}
                />
            </Link>
        </header>
    )
}