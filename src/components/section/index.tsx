import * as React from "react";
import classNames from "classnames";
import "./style.scss";

export interface SectionProps {
    children: React.ReactNode;
    title?: string;
    addTopMargin?: boolean;
    long?: boolean;
}

export function Section(props: SectionProps) {
    const sectionClass = classNames(
        "content-section",
        {
            "add-top-margin": props.addTopMargin,
            "long": props.long
        }
    )
    return (
        <section
            className={sectionClass}
            id={props.title || undefined}
        >
            {
                props.title ?
                <h4 className={"section-header bold"}>{props.title.replace("-", " ")}</h4>
                : undefined
            }
            {props.children}
        </section>
    );
}