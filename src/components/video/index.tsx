import * as React from "react";
import "./style.scss";

export interface VideoProps {
    children: React.ReactNode;
}

export function Video(props: VideoProps) {
    return (
        <div className="videoWrapper">
            {props.children}
        </div>
    )
}