import * as React from "react";
import "./style.scss";

export interface ProjectGridProps {
    children: React.ReactNode;
}

export function ProjectGrid(props: ProjectGridProps) {
    return (
    <section className="project-grid">
        {props.children}
    </section>
    );
}