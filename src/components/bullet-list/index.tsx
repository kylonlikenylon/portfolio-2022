import * as React from "react";
import "./style.scss";

export interface BulletListProps {
    list: string[];
}

export function BulletList(props: BulletListProps) {
    return (
        <ul className="bullet-list">
            {
                props.list.map((listItem: string) => {
                    return <li key={listItem}>{listItem}</li>
                })
            }
        </ul>
    );
}