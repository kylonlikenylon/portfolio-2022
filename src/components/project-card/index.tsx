import * as React from "react";
import "./style.scss";
import { InternalLink } from "../internal-link";

export interface ProjectCardProps {
    title: string;
    description: string;
    route: string;
    wip?: boolean;
}

export function ProjectCard(props: ProjectCardProps) {
    return (
        <div
            className={"project-card"}
        >
            <div
                className={"text"}
            >
                <h2 className={"title"}>{props.title}</h2>
                <p className={"desc"}>{props.description}</p>
                {
                    props.wip ?
                        <p className={"wip"}>{"(( In Progress ))"}</p>
                        :
                        <InternalLink
                            text={"Read case study"}
                            url={props.route}
                        />
                }
            </div>
        </div>
    )
}