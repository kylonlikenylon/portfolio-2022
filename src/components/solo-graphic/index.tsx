import * as React from "react";
import classNames from "classnames";
import "./style.scss";

export interface SoloGraphicProps {
    imgUrl: string;
    altText: string;
    id?: string;
    ignoreAllMargin?: boolean
    ignoreTopMargin?: boolean
    ignoreBottomMargin?: boolean
    caption?: string;
}

export function SoloGraphic(props: SoloGraphicProps) {
    const graphicClass = classNames(
        "solo-graphic",
        {
            "ignore-all-margin": props.ignoreAllMargin,
            "ignore-top-margin": props.ignoreTopMargin,
            "ignore-bottom-margin": props.ignoreBottomMargin
        }
    )

    return (
        <figure
            className={graphicClass}
            id={props.id || undefined}
        >
            <img
                src={props.imgUrl}
                alt={props.altText}
            />
            {
                props.caption ?
                    <figcaption>{props.caption}</figcaption>
                    : undefined
            }
        </figure>
    );
}