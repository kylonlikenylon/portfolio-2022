import * as React from "react";
import { animateScroll } from "react-scroll";
import { Routes, Route, useLocation } from "react-router-dom";
import { Header } from "./components/header";
import { Home } from "./pages/home";
import { Ribits } from "./pages/ribits";
import { Veracity } from "./pages/veracity";
import { BlueCeramics } from "./pages/blue-ceramics";
import { Outward } from "./pages/outward";
import { ContactInfo } from "./components/contact-info";
import { Section } from "./components/section";
import { InternalLink } from "./components/internal-link";
import { SoloGraphic } from "./components/solo-graphic";
import SelfPortrait from "./images/illustrations/self-portrait.png";
import "./app.scss";

export enum HOME_NAV {
	_bio = "bio",
	_projects = "projects",
	_contact = "contact"
};

export enum PROJECT_NAV {
	_overview = "background",
	_outcome = "outcome",
	_process = "process",
	_reflection = "reflection",
	_thanks = "special-thanks",
	_next = "up-next"
};

export enum PROJ_ROUTE {
	_root = "project",
	_ribits = "ribits",
	_veracity = "veracity",
	_blue_ceramics = "blue-ceramics",
	_outward = "outward"
}

const MOBILE_BREAKPOINT = 450;

export function App() {

	const [goesBack, setGoesBack] = React.useState(false);
	const [isMobile, setIsMobile] = React.useState(false);


	const location = useLocation();

	React.useEffect(() => {
		// Scroll back to the top
		animateScroll.scrollToTop({ duration: 250 });

		if (location.pathname.includes(PROJ_ROUTE._root)) {
			setGoesBack(true);
		} else {
			setGoesBack(false);
		}

		const resizeHandler = () => {
			if (window.innerWidth <= MOBILE_BREAKPOINT) {
				setIsMobile(true);
			} else {
				setIsMobile(false);
			}
		};

		window.addEventListener("resize", resizeHandler);
		resizeHandler();

		return () => {
			window.removeEventListener("resize", resizeHandler);
		}

	}, [location, window.innerWidth])


	return (
		<React.Fragment>
			<Header />
			<section className="content">
				<main>
					<Routes>
						<Route path={"/"}>
							<Route index element={<Home />} />
							<Route path={PROJ_ROUTE._root}>
								<Route path={PROJ_ROUTE._ribits} element={<Ribits isMobile={isMobile} />} />
								<Route path={PROJ_ROUTE._veracity} element={<Veracity isMobile={isMobile} />} />
								<Route path={PROJ_ROUTE._blue_ceramics} element={<BlueCeramics isMobile={isMobile} />} />
								<Route path={PROJ_ROUTE._outward} element={<Outward isMobile={isMobile} />} />
							</Route>
						</Route>
					</Routes>
				</main>
				{
					goesBack ?
						<InternalLink
							text={"back"}
							url={"/"}
							goofy={true}
						/>
						: <Section
							title={HOME_NAV._contact}
						>
							<ContactInfo
								methodList={
									[
										{
											text: "Resume",
											url: "https://drive.google.com/file/d/17iJwvhasktD-Axs6OkTFX_w053jglbvK/view?usp=drive_link"
										},
										{
											text: "LinkedIn",
											url: "https://www.linkedin.com/in/kylon-chiang/"
										}
									]
								}
							/>
						</Section>
				}
				{
					goesBack ?
						undefined
						: <SoloGraphic
							ignoreAllMargin={true}
							imgUrl={SelfPortrait}
							altText={"A cartonnish illustration of me looking up and to the left."}
							id={"self-portrait"}
						/>
				}
			</section>
		</React.Fragment>
	);
}