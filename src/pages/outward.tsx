import * as React from "react";
import { Helmet } from "react-helmet";
import { PROJECT_NAV, PROJ_ROUTE } from "../app";
import { Section } from "../components/section";
import { Listing } from "../components/nested-list";
import { ProjectCard } from "../components/project-card";
import { SoloGraphic } from "../components/solo-graphic";
import { BulletList } from "../components/bullet-list";
import { Subsection } from "../components/subsection";
import { ProjectGrid } from "../components/project-grid";
import { title as RibitsTitle, desc as RibitsDesc } from "./ribits";
import { title as VeracityTitle, desc as VeracityDesc } from "./veracity";
import OutwardCover from "../images/covers/outward-cover.png";
import NorthFaceSS from "../images/outward/north-face-ss.png";
import CardSorting from "../images/outward/card-sorting.png";
import Sketches from "../images/outward/sketches.png";
import Backpack360 from "../images//outward/backpack-360.png";
import Tent360 from "../images/outward/tent-360.png";
import RoomPlanner from "../images/outward/room-planner.png";
import Moodboard from "../images/outward/moodboard.png";
import Perspective from "../images/outward/perspective.png";

export interface OutwardProps {
    isMobile: boolean;
}

export const title = "Outward";
export const desc = "Led cross-functional design and development of modern interactive experiences for e-commerce.";

export function Outward(props: OutwardProps) {
    const thanks: Listing[] = [
        {
            title: "Collaborators",
            items: [
                "Monica Chang",
                "Steve Orchosky"
            ]
        },
        {
            title: "Faculty Advisors",
            items: [
                "Jason Hong",
                "Hong Chen"
            ]
        }
    ];

    const responsibilities = [
        "Led cross-functional teams to develop and release multiple projects.",
        "Conducted user research to uncover user pain points and product requirements.",
        "Built and deployed Node.js applications in React and TypeScript.",
        "Conducted usability testing to refine interface elements and interactions.",
        "Managed client communications for onboarding, feedback, and technical support."
    ];

    return (
        <React.Fragment>
            <Helmet>
                <title>Kylon ✧ In Practice</title>
            </Helmet>
            <Section
                title={title}
            >
                <h1>Modern web experiences for legacy e-commerce brands</h1>
            </Section>
            <SoloGraphic
                imgUrl={OutwardCover}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
                ignoreAllMargin={true}
            />
            <Section
                title={PROJECT_NAV._overview}
                addTopMargin={true}
            >
                <p>
                    Customer satisfaction is a constant challenge for the world of e-commerce due to a mismatch in customer expectations versus product reality. This results in product returns that impact the net revenue for sellers.
                </p>
                <p>
                    In order to close this gap, we partnered with sellers to design and develop customized digital experiences for that allow buyers to explore the product before purchasing. This way, customers know exactly what they’re getting and sellers can save on costs.
                </p>
            </Section>
            <Section
                title={"Responsibilities"}
                addTopMargin={true}
            >
                <BulletList
                    list={responsibilities}
                />
            </Section>
            <Section
                title={"Projects"}
                addTopMargin={true}
            >
                <Subsection
                    title={"The North Face 360s"}
                    smallTopMargin={true}
                >
                    <p>
                        We worked with The North Face launch an interactive 360 pilot to provide a holistic view of each product and highlight product features to help consumers feel more confident in their purchases.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={NorthFaceSS}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    I collaborated with the lead designer to plan and run research sessions where we sought to uncover the customer’s thought process behind their purchases. We found that attributes like weight and accessibility were more important than novelty features.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={CardSorting}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    With the new priorities in mind, we started to sketch ideas for the new interactive experience. The lead designer and I also worked closely to create a flexible and responsive layout that would work on all devices.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Sketches}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    After numerous iterations, we were finally ready to build. I leveraged Outward’s proprietary imaging package and wrote custom CSS and Javascript scripts to realize the vision. The MVP included two different types of products: backpacks and tents. Our research revealed that customers were more concerned with the capacity tents when shopping, so we designed an “interior view” experience to better show off the feel of each tent.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Backpack360}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <SoloGraphic
                imgUrl={Tent360}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
                ignoreTopMargin={true}
            />
            <Section>
                <p>
                    The pilot was launched in 2019 and lasted several months before it was ultimately retired. I had a lot of fun and I’m extremely humbled to have worked with such an iconic brand for one of my first projects.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"Design Crew Room Planner"}
                    smallTopMargin={true}
                >
                    <p>
                        My most notable contribution at Outward is the Design Crew Room Planner, a floor-planning web application for interior designers at Williams-Sonoma. I led a cross-functional team to develop and deploy the tool that replaced their legacy system and provided the convenience of using official WSI products.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={RoomPlanner}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    I collaborated with the design team to iterate and implement the interface. But we soon noticed that while the new tool certainly looked more sleek than the older system, it was wasn’t gaining traction amongst our target users.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Moodboard}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Talking to the interior designers helped us realize that most customers were more interested in  inspirational mood boards as opposed to top-down plans, so we set to designing and adding features that made Room Planner more useful to the consumer.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Perspective}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    By the time I departed Outward, Design Crew Room Planner was helping designers to bring in 3x the amount of revenue than their counterparts who continued to use the legacy system. It was a great honor to lead and contribute to such a high impact project.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._reflection}
                addTopMargin={true}
            >
                <p>
                    I am forever grateful for the opportunity I was given at Outward, especially to my managers and collaborators who trusted me with the task of running cross-functional teams. My capabilities as an individual contributor and leader were able to blossom under this challenging and fast-paced environment.
                </p>
                <p>
                    While this is the end of a chapter, it’s just the beginning of my story. There are many more problems in the world that need our care and attention more than ever, and I look forward to dedicating my energy to these endeavors.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._next}
                addTopMargin={true}
            >
                <ProjectGrid>
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._ribits}`}
                        title={RibitsTitle}
                        description={RibitsDesc} />
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._veracity}`}
                        title={VeracityTitle}
                        description={VeracityDesc} />
                </ProjectGrid>
            </Section>
        </React.Fragment>
    )
}