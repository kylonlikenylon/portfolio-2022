import * as React from "react";
import { Helmet } from "react-helmet";
import { PROJECT_NAV, PROJ_ROUTE } from "../app";
import { Section } from "../components/section";
import { NestedList, Listing } from "../components/nested-list";
import { Subsection } from "../components/subsection";
import { ProjectCard } from "../components/project-card";
import { SoloGraphic } from "../components/solo-graphic";
import { BulletList } from "../components/bullet-list";
import { Video } from "../components/video";
import { ProjectGrid } from "../components/project-grid";
import { title as RibitsTitle, desc as RibitsDesc } from "./ribits";
import { title as OWTitle, desc as OWDesc } from "./outward";
import BlueCeramicsCover from "../images/covers/blue-ceramics-cover.png";
import Meadow from "../images/blue-ceramics/meadow.png";
import Ecosystem from "../images/blue-ceramics/ecosystem.png";
import Sketches from "../images/blue-ceramics/sketches.png";
import Models from "../images/blue-ceramics/models.png";
import Underwater from "../images/blue-ceramics/underwater.png";
import MorphingSamples from "../images/blue-ceramics/morphing-samples.png";
import FabMethods from "../images/blue-ceramics/fabrication-methods.png";
import GrooveDensity from "../images/blue-ceramics/groove-density.png";
import SedTest from "../images/blue-ceramics/sediment-test.png";

export interface BlueCeramicsProps {
    isMobile: boolean;
}

export const title = "Blue Ceramics";
export const desc = "Guided research and fabrication of clay structures for restoring declining oceanic ecosystems.";

export function BlueCeramics(props: BlueCeramicsProps) {

    const thanks: Listing[] = [
        {
            title: "Collaborators",
            items: [
                "Rachel Arredondo",
                "Ofri Dar"
            ]
        },
        {
            title: "Morphing Matters Lab",
            items: [
                "Prof. Lining Yao",
                "Dinesh Patel",
                "Jianzhe Gu",
                "Kexin Lu"
            ]
        },
        {
            title: "Marine Experts",
            items: [
                "Dr. Abbey Engelman",
                "Dr. Jesse Jarvis",
                "Jeffery Good",
                "Dr. Emmett Duffy"
            ]
        },
        {
            title: "Hebrew University of Jerusalem",
            items: [
                "Prof. Eran Sharon",
                "Dr. Arielle Blonder",
                "Shira Shoval"
            ]
        },
        {
            title: "Bezalel Academy of Art and Design",
            items: [
                "Noam Dover",
                "Prof. Haim Parnes"
            ]
        },
        {
            title: "Fireborn Studios",
            items: [
                "Dan Vito",
                "Donna Hetrick",
                "Bennett Graves"
            ]
        }
    ];

    const responsibilities = [
        "Planned and led interviews to discover the factors contributing to the the decline of seagrass meadows.",
        "Co-led participatory design sessions with marine scientists to collaboratively brainstorm solutions.",
        "Designed and digitally fabricated physical prototype of ceramic structures.",
        "Conducted current tests to ensure effectiveness in underwater environments.",
        "Co-authored a published ACM pictorial to share our work with the world!"
    ];

    return (
        <React.Fragment>
            <Helmet>
                <title>Kylon ✧ In Practice</title>
            </Helmet>
            <Section
                title={title}
            >
                <h1>Conservation tool for restoring seagrass meadows</h1>
            </Section>
            <SoloGraphic
                imgUrl={BlueCeramicsCover}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
                ignoreAllMargin={true}
            />
            <Section
                title={PROJECT_NAV._overview}
                addTopMargin={true}
            >
                <p>
                    Seagrass meadows have been in steady decline for the past few decades due to human activities. However, most existing conservation tools are cumbersome to deploy and only focus on addressing one problem in a complex ecosystem.
                </p>
                <p>
                    My team in the Morphing Matter Lab partnered with marine scientists and industrial designers to investigate how we can build a simple and effective conservation tool to revive the Earth’s seagrass population.
                </p>
            </Section>
            <Section
                title={"Responsibilities"}
                addTopMargin={true}
            >
                <BulletList
                    list={responsibilities}
                />
            </Section>
            <Section
                title={PROJECT_NAV._outcome}
                addTopMargin={true}
            >
                <p>
                    Over the course of 6 months, my team designed a self-deploying modular ceramic tile system. These structures  trap sediment and reduce current forces in order to halt further erosion of seagrass meadows.
                </p>
            </Section>
            <Video>
                <iframe
                    className="video"
                    width="560" height="315"
                    src="https://www.youtube.com/embed/iZyPpMNwwLM"
                    title="YouTube video player"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </Video>
            <Section>
                <p>
                    Moreover, The use of natural materials like clay means there will be no need for clean-up—the structures will gradually break down and become part of the environment once again.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._process}
                addTopMargin={true}
            >
                <Subsection
                    title={"An Ecosystem In Peril"}
                    smallTopMargin={true}
                >
                    <p>
                        Much of the focus of coastal restoration has been on corals. Yet, there’s another organism that provides equally as many benefits to marine ecosystems and receives far less attention—the humble seagrass.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={Meadow}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Studies have shown that seagrass meadows are many times more effective than rainforests at carbon capture and storage, so they will be a key player in the coming decades as we face climate change.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Ecosystem}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Our interviews with marine experts and reviews of scientific papers indicated that meadow erosion starts off in small patches, or “cold spots”. Strong currents rush through these areas and disturb the sediment, which clouds the water and obscures necessary sunlight for seagrass survival and causes more seagrass to die off.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"Bright Ideas"}
                >
                    <p>
                        I co-led interview sessions with marine experts where we adapted a participatory protocol for developing IoT products to center a natural stakeholder instead of humans. The prompts helped to surface the deep knowledge of our experts, and they partnered with us to live sketch some possible interventions.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={Sketches}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    The industrial designer on our team used these sketches to create an initial form in CAD software to inform our fabrication process. We iterated on this design as our understanding of the conditions and constraints also shifted.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"Envisioning The Future"}
                >
                    <p>
                        Using the CAD files, I built an environment in Unreal Engine to get quick feedback on form details from marine experts. Once we knew we had something viable, it was time to start building.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={Models}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"To Soil We Return"}
                >
                    <p>
                        I guided our team’s exploration of the fabrication process by building on previous work in morphing ceramics.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={Underwater}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Each type of clay has a different shrinkage rate, so we took advantage of this property by joining 2 types of clay back-to-back as one tile and putting them in the oven. Since one clay inevitably shrinks more than the other, the tension causes the entire piece to bend.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={MorphingSamples}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    The tiles were made in one of two ways: CNC grooving and 4D printing. CNC grooving used hand-formed clay and a plotter machine to carve grooves into the clay where it was meant to bend. On the other hand, 4D printing was completely mechanical and used a commercial 3D printer to precisely create each tile.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={FabMethods}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    I tested various parameters to manipulate the curvature by adjusting factors such as tile size, clay thickness, and groove density.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"Moment Of Truth"}
                >
                    <p>
                        After completing a few rounds of fabrication tests, we were ready to deploy the structures for an initial test. However, field testing requires lots of set-up, so my team and I built a pseudo-wave tank to test our designs in the lab.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={GrooveDensity}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"You Reap What You Sow"}
                >
                    <p>
                        We also explored the possibility of deploying these structures with seagrass seeds embedded into the grooves to help with repopulation. We created several special needle tips and saw promising results!
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={SedTest}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section
                title={PROJECT_NAV._reflection}
                addTopMargin={true}
            >
                <p>
                    I am truly grateful to have this opportunity to work on such an impactful project with an incredible team. I look forward to taking on similar projects since we are at a vital point in history and I want to devote my energy to the prosperity of future generations and other species.
                </p>
                <p>
                    This project reaffirmed my belief that interdisciplinary work is the way forward. Our project stands on the shoulders of countless experts from around the world; none of this work would have been possible without their knowledge and expertise.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._thanks}
                addTopMargin={true}
            >
                <NestedList
                    listOfList={thanks}
                />
            </Section>
            <Section
                title={PROJECT_NAV._next}
                addTopMargin={true}
            >
                <ProjectGrid>
                <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._outward}`}
                        title={OWTitle}
                        description={OWDesc} />
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._ribits}`}
                        title={RibitsTitle}
                        description={RibitsDesc} />
                </ProjectGrid>
            </Section>
        </React.Fragment>
    );
}