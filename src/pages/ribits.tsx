import * as React from "react";
import { Helmet } from "react-helmet";
import { PROJECT_NAV, PROJ_ROUTE } from "../app";
import { Section } from "../components/section";
import { NestedList, Listing } from "../components/nested-list";
import { Subsection } from "../components/subsection";
import { ProjectCard } from "../components/project-card";
import { SoloGraphic } from "../components/solo-graphic";
import { BulletList } from "../components/bullet-list";
import { ProjectGrid } from "../components/project-grid";
import { title as VeracityTitle, desc as VeracityDesc } from "./veracity";
import { title as BlueCTitle, desc as BlueCDesc } from "./blue-ceramics";
import Wetland from "../images/ribits/wetland.png";
import Preview from "../images/ribits/mock-up-preview.png";
import AffinityDiagram from "../images/ribits/affinity-diagram.png";
import Controls from "../images/ribits/controls.png";
import JourneyMap from "../images/ribits/journey-map.png";
import ReportExamples from "../images/ribits/report-examples.png";
import FilterSketch from "../images/ribits/filter-sketch.png";
import TooltipSketch from "../images/ribits/tooltip-sketch.png";
import DownloadSketch from "../images/ribits/download-sketch.png";
import FaceOff from "../images/ribits/face-off.png";
import TooltipMock from "../images/ribits/tooltip-mock.png";
import AddColumnMock from "../images/ribits/add-column-mock.png";
import FilterMock from "../images/ribits/filter-mock.png";
import DownloadMock from "../images/ribits/download-mock.png";

export interface RibitsProps {
    isMobile: boolean;
};

export const title = "RIBITS";
export const desc = "Directed research and redesign for an essential federal platform to track wetland and habitat conservation.";

export function Ribits(props: RibitsProps) {

    const thanks: Listing[] = [
        {
            title: "Collaborators",
            items: [
                "Jessie Mahr",
                "Shelby Switzer",
                "Sijia Pitts",
                "Tiffany Kwak",
                "Vim Shah"
            ]
        }
    ];

    const responsibilities: string[] = [
        "Planned and conducted user research to draw out common workflows and causes for frustration.",
        "Facilitated synthesis sessions to discuss insights and inform the direction for improvements.",
        "Led brainstorming workshops with cross-functional team to sketch and discuss feasible solutions.",
        "Designed an updated workflow and interface using findings derived from research data.",
        "Validated design with target audience and leveraged user feedback to demonstrate value of UX changes.",
        "Documented process from definition to delivery to serve as guide for future UX projects."
    ];

    return (
        <React.Fragment>
            <Helmet>
                <title>Kylon ✧ In Practice</title>
            </Helmet>
            <Section
                title={title}
            >
                <h1>User experience updates for a federal conservation database</h1>
            </Section>
            <SoloGraphic
                imgUrl={Wetland}
                altText={""}
                ignoreAllMargin={true}
            />
            <Section
                title={PROJECT_NAV._overview}
                addTopMargin={true}
            >
                <p>
                    The Regulatory In-Lieu Fee Program and Bank Information Tracking System (RIBITS) enables national-scale wetland and habitat conservation work, yet local government agencies across the country are hesitant to adopt the system because they’ve had many frustrating experiences with the platform. Moreover, the US Army Corps of Engineers (USACE) lacks proper resources and know-how to make improvements, causing many stakeholders to abandon RIBITS altogether.
                </p>
                <p>
                    In order to break this trend, the Environmental Policy Innovation Center (EPIC) assembled a team of open-source UX specialists to explore common user frustrations with RIBITS. The goal was not only to put forth concrete recommendations for updates, but to create a blueprint for future human-centered software development practices at USACE.
                </p>
            </Section>
            <Section
                title={"Responsibilities"}
                addTopMargin={true}
            >
                <BulletList
                    list={responsibilities}
                />
            </Section>
            <Section
                title={PROJECT_NAV._outcome}
                addTopMargin={true}
            >
                <p>
                    My team distilled our research findings into a prototype that features a common task among RIBITS users—data search and download. Our design increases the legibility and discoverability of data by including definitions and surfacing hidden controls that allow users to quickly and easily access the data they need.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Preview}
                ignoreAllMargin={true}
                altText={""}
            />
            <Section>
                <p>
                    We also included a process write-up that detailed our methodologies and provided links/images to our design artifacts. We intend for these deliverables to serve as references in future UX improvement initiatives within USACE.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._process}
                addTopMargin={true}
            >
                <Subsection
                    title={"Adjusting Focus"}
                    smallTopMargin={true}
                >
                    <p>
                        This project started as an open exploration of the common pain points for various user groups of RIBITS. However, I quickly realized the vastness of the platform and recommended scoping down to narrow the focus of our work to the pieces that would make the most compelling case for the Army Corps.
                    </p>
                    <p>
                        We ultimately decided that various state-level agencies and organizations that depend on RIBITS to drive conservation and entrepreneurial efforts should be our primary audience; their input would hold great sway it comes to convincing USACE of the importance of our work.
                    </p>
                </Subsection>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"Under the Microscope"}
                >
                    <p>
                        Having identified a target user group, we set out to understand their hesitations about using RIBITS. I collaborated with the UX Researcher to develop an interview format that prompted participants to demonstrate their most common workflow and prominent pain points with RIBITS.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={AffinityDiagram}
                altText={""}
            />
            <Section>
                <p>
                    Due to time constraints, we conducted fewer interviews than what would’ve been ideal. But we’d already started to see patterns in people’s concerns about information architecture, documentation, and data quality. We leaned into improving information architecture and documentation since data issues would have to be addressed through policy.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Controls}
                altText={""}
            />
            <Section>
                <p>
                    In our own investigation of RIBITS, we discovered that although the tool was clunky, many of functionalities that users indicated were complicated only seemed so because they were buried within menus or had unclear labels. This signaled to me that our work should restructure the website and bring these functionalities to the surface.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"The Broader View"}
                >
                    <p>
                        In parallel with our data synthesis, I also built a user journey map in order to reveal more detailed areas of opportunity. We decided to focus on the data search and download workflow, since it was a common task shared across the research participants.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={JourneyMap}
                altText={""}
            />
            <Section>
                <p>
                    Through this exercise, it became clear to me that users were also engaging in a lot of repetitive work. RIBITS has no way to preview the file prior to download, so they must download each file, check the contents, fiddle with the options, then repeat the process. Even just observing our participants made me feel frustrated.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={ReportExamples}
                altText={""}
            />
            <Section>
                <p>
                    I started to dig into the data search function and it occurred to me that the information provided in each view was more or less the same. This observation was later confirmed by the development team, and we figured that all the views could be consolidated into singular location with an improved filter and download experience, we could save users a lot of effort.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"Sketchy Ways"}
                >
                    <p>
                        With this epiphany in mind, I started to sketch alternative experiences to search and download data on RIBITS. I started by designing a universal search function for all data on RIBITS. It didn’t make sense to split the data into different views when they were all related.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={FilterSketch}
                altText={""}
            />
            <Section>
                <p>
                    From our research, we also learned that users come to RIBITS with some sense of the data they need, but aren’t sure how to find that information. So I made sketches for a filter function that displayed all data categories to improve data visibility. Additionally, users can hover over the title of each column for a(n) explanation/definition to help them better understand the data and their values.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={TooltipSketch}
                altText={""}
            />
            <Section>
                <p>
                    Additionally, users can hover over the title of each column for a(n) explanation/definition to help them better understand the data and their values.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={DownloadSketch}
                altText={""}
            />
            <Section>
                <p>
                    Lastly, I redesigned the data download panel to give users control over which columns to include at download time. This interaction also serves as a preview to help them avoid do-overs when downloading from RIBITS.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"Building A Vision"}
                >
                    <p>
                        I presented my sketches in a collaborative brainstorming session, and it turned our whole team was pretty much on the same wavelength. This made consolidating our ideas much smoother, and we were able to quickly arrive at consensus and move to a higher fidelity.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={FaceOff}
                altText={""}
            />
            <Section>
                <p>
                    The one interaction we couldn’t agree on was how users want to filter data categories, using boolean operators vs manual selection/deselection, so we decided to include both versions in the prototype for A/B testing when we evaluate our ideas with users.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title={"Review and Reorient"}
                >
                    <p>
                        Our proposed changes were met with enthusiasm, but since the scope of our project was limited, it was also difficult to measure the impact that this redesign could make. However, taking the USACE’s capacity into consideration, we decided to stick with the current course. If they were agreeable, then we would recommend larger-scale changes.
                    </p>
                    <p>
                        (( Review of changes are ongoing))
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={TooltipMock}
                altText={""}
                ignoreBottomMargin={true}
            />
            <SoloGraphic
                imgUrl={AddColumnMock}
                altText={""}
                ignoreAllMargin={true}
            />
            <SoloGraphic
                imgUrl={FilterMock}
                altText={""}
                ignoreAllMargin={true}
            />
            <SoloGraphic
                imgUrl={DownloadMock}
                altText={""}
                ignoreTopMargin={true}
            />
            <Section
                title={PROJECT_NAV._reflection}
                addTopMargin={true}
            >
                <p>
                    I was first drawn to this project because of my personal interest in policy and conservation, but what I didn’t anticipate was the scale and complexity of redesigning a long-standing system—yet it turned out to be the perfect challenge for me. This project gave me the opportunity to drive the scoping process to get into the nitty gritty of the details for a singular workflow. I believe the work that my team did on the data search and download feature could easily be used as the basis for a design system and applied across the platform.
                </p>
                <p>
                    I wish we had more time to do discovery research since RIBITS is a large and complex system that requires effort to understand. Our team started in the right direction, but I don’t think we had enough depth of information to propose and entire re-design, and that wasn’t the brief anyway. My hope is that we set good enough of a precedence that the USACE will see value in a human-centered design process and pursue this methodology as they decide the fate of RIBITS.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._thanks}
                addTopMargin={true}
            >
                <NestedList
                    listOfList={thanks}
                />
            </Section>
            <Section
                title={PROJECT_NAV._next}
                addTopMargin={true}
            >
                <ProjectGrid>
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._veracity}`}
                        title={VeracityTitle}
                        description={VeracityDesc} />
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._blue_ceramics}`}
                        title={BlueCTitle}
                        description={BlueCDesc} />
                </ProjectGrid>
            </Section>
        </React.Fragment>
    );
}