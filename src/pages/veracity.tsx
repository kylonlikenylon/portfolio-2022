import * as React from "react";
import { Helmet } from "react-helmet";
import { PROJECT_NAV, PROJ_ROUTE } from "../app";
import { Section } from "../components/section";
import { NestedList, Listing } from "../components/nested-list";
import { Subsection } from "../components/subsection";
import { ProjectCard } from "../components/project-card";
import { SoloGraphic } from "../components/solo-graphic";
import { Video } from "../components/video";
import { BulletList } from "../components/bullet-list";
import VeracityCover from "../images/covers/veracity-cover.png";
import RepellingIdeas from "../images/veracity/repelling-ideas.png";
import EchoChambers from "../images/veracity/echo-chambers.png";
import CrumblingInstitutions from "../images/veracity/crumbling-institutions.png";
import Audience from "../images/veracity/audience.png";
import Pretotype from "../images/veracity/pretotype.png";
import Engagement from "../images/veracity/engagement.png";
import Storyboards from "../images/veracity/storyboards.png";
import Evaluation from "../images/veracity/evaluation.png";
import Reflection from "../images/veracity/reflection.png";
import Action from "../images/veracity/action.png";
import Customization from "../images/veracity/customization.png";
import Sketches from "../images/veracity/sketches.png";
import Quiz from "../images/veracity/quiz.png";
import Onboarding from "../images/veracity/onboarding.png";
import EvalPrototype from "../images/veracity/eval-prototype.png";
import ReflectionPrototype from "../images/veracity/reflection-prototype.png";
import ActionPrototype from "../images/veracity/action-prototype.png";
import CustomPrototype from "../images/veracity/customization-prototype.png";
import { ProjectGrid } from "../components/project-grid";
import { title as BlueCTitle, desc as BlueCDesc } from "./blue-ceramics";
import { title as OWTitle, desc as OWDesc } from "./outward";

export interface VeracityProps {
    isMobile: boolean;
}

export const title = "Veracity";
export const desc = "Conducted research, design, and validation for AI tools to mitigate misinformation.";

export function Veracity(props: VeracityProps) {

    const thanks: Listing[] = [
        {
            title: "Collaborators",
            items: [
                "Amy Zhuang",
                "Alexandra Hopping",
                "Janelle Wen",
                "Rachel Arredondo"
            ]
        },
        {
            title: "Faculty Advisors",
            items: [
                "Anna Abovyan",
                "Raelin Musuraca"
            ]
        },
        {
            title: "PNNL Partners",
            items: [
                "Dustin Arendt",
                "Maria Glenski"
            ]
        }
    ]

    const responsibilities = [
        "Planned and led interviews to discover the factors contributing to the spread of misinformation.",
        "Synthesized research to form insights that drove project exploration.",
        "Designed and prototyped mobile app screens for suite of interventions.",
        "Conducted usability testing and refined prototypes based on feedback.",
        "Facilitated weekly client check-ins to discuss project progress and collaboratively make decisions."
    ];

    return (
        <React.Fragment>
            <Helmet>
                <title>Kylon ✧ In Practice</title>
            </Helmet>
            <Section
                title={title}
            >
                <h1>Human-AI collaborative tools to mitigate misinformation</h1>
            </Section>
            <SoloGraphic
                imgUrl={VeracityCover}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
                ignoreAllMargin={true}
            />
            <Section
                title={PROJECT_NAV._overview}
                addTopMargin={true}
            >
                <p>
                    In recent years, we’ve experienced an uncontrolled spread of false information across social platforms. And although large platforms have started to deploy their own solutions, they’re still falling short of handling the problem.
                </p>
                <p>
                    In order to investigate this issue, my team partnered with senior AI researchers and data scientists at The Pacific Northwest National Laboratory (PNNL) to explore how we can leverage AI capabilities to help everyday people mitigate misinformation.
                </p>
            </Section>
            <Section
                title={"Responsibilities"}
                addTopMargin={true}
            >
                <BulletList
                    list={responsibilities}
                />
            </Section>
            <Section
                title={PROJECT_NAV._outcome}
                addTopMargin={true}
            >
                <p>
                    As we wait for technology, policy, and culture to catch up, <em>AI presents the greatest potential for immediate positive effects</em> on limiting the harms of misinformation.
                </p>
            </Section>
            <Video>
                <iframe
                    className="video"
                    src="https://player.vimeo.com/video/643066870?h=8aa692f680"
                    frameBorder="0"
                    allow="autoplay; fullscreen; picture-in-picture"
                    allowFullScreen
                ></iframe>
            </Video>
            <Section>
                <p>
                    My team used data from our research to put forth a set of human-centered design guidelines to help social platforms develop AI-driven interventions to mitigate the spread and consumption of misinformation. We also crafted a set of prototypes to accompany these guidelines to demonstrate these principles in practice.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._process}
                addTopMargin={true}
            >
                <Subsection
                    title={"A Crumbling Foundation"}
                    smallTopMargin={true}
                >
                    <p>
                        We kicked off the project by interviewing experts in AI, misinformation, and social computing to build a foundational understanding of the problem. From the expert point of view, misinformation is a complex mixture of psychological, technological, and social roots.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={RepellingIdeas}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section
            >
                <p>
                    Psychologically, humans tend reject information that doesn’t agree with their existing viewpoint. I witnessed this behavior first-hand during think-aloud sessions with social platform users. Participants were more dismissive of information that didn’t align with their perspective or interest, regardless of the truth.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={EchoChambers}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section
            >
                <p>
                    Technologically, algorithms have created echo chambers that are difficult to escape and present unilateral perspectives of controversial topics. This makes misinformation hard to identify and refute, and radicalizes sectors of our society.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={CrumblingInstitutions}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section
            >
                <p>
                    Socially, trust between people and established institutions is unraveling. Waning faith in the government and news outlets has caused people to turn to alternative sources for information, which can be credited for the rise of conspiracy theory groups like QAnon.
                </p>
                <p>
                    These problems are bigger than any that could be handled by a team of designers, but we must also recognize that change doesn’t take place overnight; meaningful change occurs incrementally, and we needed to play our role to inch progress along.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title="who this is for"
                >
                    <p>
                        Initially, we hoped to create interventions that would target people who spread misinformation to stop the problem at the source. However, it was difficult to reach these people in the first place, and probably even harder to changed their minds
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={Audience}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section
            >
                <p>
                    So we worked with our partners to scope the target users to anyone who unintentionally spreads or consumes misinformation. Statistically speaking, misinformation comes from a relatively small number of people, so if we eliminate their audience, then we also effectively reduce the harms of misinformation.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title="Setting The Scene"
                >
                    <p>
                        I co-led interview sessions using an early-stage prototype to elicit from users the types of resources that they believe would be helpful to fight misinformation.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={Pretotype}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    During these sessions, we observed an intriguing contradiction between users asking for more sources yet lacking motivation to see their research all the way through. Generally, users were more likely to investigate a piece of information if the content felt relevant to them.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Engagement}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    We mapped this behavior into what we coined as the “Engagement Model.” We realized that if we could design to retrain user behaviors on social platforms, then we could nip the problem in the bud.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title="Out Of The Comfort Zone"
                >
                    <p>
                        To be frank, our team disagreed on the use of AI. Our earlier research revealed a general negative attitude towards AI, and I personally believe that more technology won’t get us out of this situation. However, given the speed and scale of misinformation spread, it seemed like a viable option to explore.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={Storyboards}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Given most people’s skeptical attitudes towards AI, we started by probing their comfort levels. I led multiple speed dating sessions and had participants respond to various instance where AI was use handle misinformation.
                </p>
                <p>
                    We found that while there was an aversion to AI, users were more receptive if they felt that the system was helping them achieve a goal in some way. Therefore, understanding these goals was paramount to designing effective interventions
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title="Recipe For Change"
                >
                    <p>
                        The goals we identified can be broadly categorized into four separate categories: evaluation, reflection, action, and customization.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={Evaluation}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Evaluation is concerned with helping users to quickly judge the veracity of the information that they’re engaging with, and providing the necessary tools to take an informed on the topic.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Reflection}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Reflection helps users parse through and digest their activity on social platforms, and surfaces actionable insights that improve the way they engage in online spaces.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Action}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Action gives users the tools to fight back against misinformation. This could take any form, such as flagging accounts that disseminate misinformation.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Customization}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Customization ensures that the interventions remain relevant and interesting to the user, so they don’t become yet another technology vying for their attention.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title="Putting Pen to Paper"
                >
                    <p>
                        Taking into account the engagement and intervention guidelines, we started to sketch out some ideas for human-AI collaborative interventions.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={Sketches}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    My sketches focused on media literacy by warning users about possible exposure to misinformation. One idea relegated the handling of misinformation to the platform by identifying hot topics, then “throttling” all messages that discuss said hot topic. However, this idea did not test well with users because there were concerns over censorship and inappropriate throttling in case of emergencies.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={Quiz}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Another idea was “corrections”, which would use AI to detect user engagement with certain topics and dispel misinformation with official facts. This idea gained traction among our research participants because they liked being kept up to date with the latest information.
                </p>
                <p>
                    This idea joined a cohort of several other concepts popular with our research participants to form the “Engagement Hub”. We viewed this as the central location for users to come and explore different aspects of their engagement on social platforms.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title="Refine, Refine, Refine"
                >
                    <p>
                        As we tested our prototype, we noticed that users were often confused by these new and unfamiliar screens and unsure of how to proceed. So we added onboarding screens to introduce the various components of the intervention.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={Onboarding}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Furthermore, we added explanations for each component to demystify the “blackbox” AI. This was a crucial and necessary addition because we found through research that the opaqueness of most AI systems caused people to mistrust the AI. Therefore, our intervention erred on the side of transparency to reassure users of best intentions.
                </p>
            </Section>
            <Section
                addTopMargin={true}
            >
                <Subsection
                    title="The Final Act"
                >
                    <p>
                        After several more rounds of testing and refinement, we had finally arrived at a point where we believed that our prototype was ready to enter the world and be tested.
                    </p>
                </Subsection>
            </Section>
            <SoloGraphic
                imgUrl={EvalPrototype}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    The in-feed veracity indicator displays the verdict of a claim accompanied by supporting facts and resources. These resources can be adjusted to fit user preferences.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={ReflectionPrototype}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    The data dashboard highlights user engagement with misinformation over time, and invites user to take charge to mitigate the amount of misinformation in the feed.
                </p>

            </Section>
            <SoloGraphic
                imgUrl={ActionPrototype}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    They have the option to either block or mute users who have been identified by the AI to be spreading misinformation, and/or remove engagement with content that was later declared false by trusted sources.
                </p>
            </Section>
            <SoloGraphic
                imgUrl={CustomPrototype}
                altText={"Graphic recreactions of various notifications employed by social platforms to warn their users about misinformation."}
            />
            <Section>
                <p>
                    Lastly, the AI customizes the content to fit individual interests so that users never feel like they’re being alerted about irrelevant content.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._reflection}
                addTopMargin={true}
            >
                <p>
                    The scope of this project was an incredible challenge, and I still have a hard time wrapping my head around it after all these months. Nonetheless, I’m grateful to have had the opportunity to work with an incredible team of people on such an important and relevant issue.
                </p>
                <p>
                    While I think my team stepped up to the challenge, I think that our solution feels more reactive than proactive. In my opinion, we need to pursue more holistic solutions that require us to rethink social platforms altogether and move to a new model of virtual interactions. Misinformation will always be around, and now the we have this lesson, we need to get ahead of it before it overtakes us once again.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._thanks}
                addTopMargin={true}
            >
                <NestedList
                    listOfList={thanks}
                />
            </Section>
            <Section
                title={PROJECT_NAV._next}
                addTopMargin={true}
            >
                <ProjectGrid>
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._blue_ceramics}`}
                        title={BlueCTitle}
                        description={BlueCDesc} />
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._outward}`}
                        title={OWTitle}
                        description={OWDesc} />
                </ProjectGrid>
            </Section>
        </React.Fragment>
    );
}