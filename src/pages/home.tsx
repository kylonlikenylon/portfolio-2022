import * as React from "react";
import { Helmet } from "react-helmet";
import { HOME_NAV, PROJ_ROUTE } from "../app";
import { Section } from "../components/section";
import { ProjectCard } from "../components/project-card";
import { ProjectGrid } from "../components/project-grid";
import { title as RibitsTitle, desc as RibitsDesc } from "./ribits";
import { title as VeracityTitle, desc as VeracityDesc } from "./veracity";
import { title as BlueCTitle, desc as BlueCDesc } from "./blue-ceramics";
import { title as OWTitle, desc as OWDesc } from "./outward";

export function Home() {


    return (
        <React.Fragment>
            <Helmet>
                <title>Kylon ✧ In Theory</title>
            </Helmet>
            <Section
                title={HOME_NAV._bio}
            >
                <p>
                    Kylon Chiang is an <em>end-to-end designer</em> by trade, engineer by training, and artist at heart seeking to create thoughtful and holistic experiences for the advancement of all living beings.
                </p>
            </Section>
            <Section
                title={"projects"}
                long={true}
                addTopMargin={true}
            >
                <ProjectGrid>
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._ribits}`}
                        title={RibitsTitle}
                        description={RibitsDesc} />
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._veracity}`}
                        title={VeracityTitle}
                        description={VeracityDesc} />
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._blue_ceramics}`}
                        title={BlueCTitle}
                        description={BlueCDesc} />
                    <ProjectCard
                        route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._outward}`}
                        title={OWTitle}
                        description={OWDesc} />
                </ProjectGrid>
            </Section>
        </React.Fragment>
    );
}