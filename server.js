"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var path_1 = __importDefault(require("path"));
var server = (0, express_1["default"])();
var servePath = path_1["default"].join(__dirname, "build");
var port = process.env.PORT || 3000;
server.use(express_1["default"].static(servePath));
server.set("port", port);
server.get('*', function (req, res) {
    res.sendFile(path_1["default"].join(servePath, "index.html"));
});
server.listen(server.get("port"), function () {
    console.log("App running on port ", port);
});
